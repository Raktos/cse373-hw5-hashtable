
public class Test {

	public static void main(String[] args) {
		FileInput.init();
		
		//TODO Initialize the hash tables
//		ChainingHash hamletTable = new ChainingHash(5387);
//		ChainingHash baconTable = new ChainingHash(5387);
		QPHash hamletTable = new QPHash(5387);
		QPHash baconTable = new QPHash(5387);

		
		//TODO Use the FileInput functions to read the two files.
		// Input the elements of those two files into two hash tables,
		// one file into a ChainingHash object and the other into a QPHash object.
		String[] hamlet = FileInput.readShakespeare();
		for(int i = 0; i < hamlet.length; i++){
			hamletTable.insert(hamlet[i]);
		}
		
		String[] bacon = FileInput.readBacon();
		for(int j = 0; j < bacon.length; j++){
			baconTable.insert(bacon[j]);
		}
		
		//TODO Initialize necessary variables for calculating the square error
		// and most distant word.
		double sqError = 0;
		String highErrorWord = "";
		double highError = -1;
		
		//TODO Iterate through the first hash table and add sum the squared error
		// for these words.
		
		//set to first word of Hamlet
		String word = hamletTable.getNextKey();
		while(word != null){ //loop while we still have words left in Hamlet
			
			//get the counts and calculate the error for this word
			double hamletCount = hamletTable.findCount(word);
			double baconCount = baconTable.findCount(word);
			double thisError = (hamletCount/(hamlet.length) - baconCount/bacon.length) * (hamletCount/hamlet.length - baconCount/bacon.length);
			
			//if this is the highest error so far, remember it
			if(thisError > highError){
				highError = thisError;
				highErrorWord = word;
			}
			
			//add to total error
			sqError += thisError;
			
			//get next word of Hamlet
			word = hamletTable.getNextKey();
		}
		
		//TODO Find  words in the second hash table that are not in the first and add their squared error
		// to the total
		
		//set to first word of Bacon
		word = baconTable.getNextKey();
		while(word != null){ //loop while we still have words left in Bacon
			if(hamletTable.findCount(word) <= 0){ //only add this word to total error if it isn't in Hamlet (if it is we already did it)
				
				//get the count and calculate the error for this word
				double baconCount = baconTable.findCount(word);
				double thisError = (baconCount/bacon.length) * (baconCount/bacon.length);
				
				//if this is the highest error so far, remember it
				if(thisError > highError){
					highError = thisError;
					highErrorWord = word;
				}
				
				//add to total error
				sqError += thisError;
			}
			
			//get next word of Bacon
			word = baconTable.getNextKey();
		}
		
		//TODO Print the total calculated squared error for the two tables and also the word with the highest distance.
		
		System.out.println("Most different square difference: '" + highErrorWord + "' with a square difference of " + highError);
		System.out.println("Total comparative error: " + sqError);
	}

}
