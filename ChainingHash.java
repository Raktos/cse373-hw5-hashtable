public class ChainingHash {
	Node[] hashTable;
	Node lastNode;
	
	private class Node{
		String key;
		int count;
		Node next;
		
		private Node(String k){
			this.key = k;
			this.count = 1;
			this.next = null;
		}
	}
	
	public ChainingHash(){
		//TODO Implement a default constructor for ChainingHash
		this(1811);
	}	

	public ChainingHash(int startSize){
		//TODO Implement a constructor that instantializes the hash array to startSize.
		this.hashTable = new Node[startSize];
	}

	/**
	 * This function allows rudimentary iteration through the ChainingHash.
	 * The ordering is not important so long as all added elements are returned only once.
	 * It should return null once it has gone through all elements
	 * @return Returns the next element of the hash table. Returns null if it is at its end.
	 */
	public String getNextKey(){
		//TODO returns the next key in the hash table.
		//You will need external tracking variables to account for this.
		Node n;
		
		if(lastNode == null){ //we're just starting, find the first Node
			n = getFirstNode();
		}else{ //not just starting, get Node after lastNode
			n = getNextNode(lastNode);
		}
		
		if(n == null){ //Node after lastNode is null, we reached the end of the table
			return null;
		}
		return n.key; //return the key of the Node we found
	}
	
	/**
	 * Find the Node after n
	 * @param n: the Node from which we will find the next Node
	 * @return Returns the next Node in the table. Returns null if there isn't another Node past n
	 */
	private Node getNextNode(Node n){
		if(n.next != null){ //n is not the end of a linked list, return the Node after it
			n = n.next;
		}else{ //n is the last Node in its list, move to next hash position
			int hash = hash(n.key); //find the position n is at
			hash++; //go to one slot past n's position
			
			boolean foundNewNode = false;
			while(!foundNewNode && hash < hashTable.length){ //loop while we have not found a new Node, and while we have not past the end of the table
				if(hashTable[hash] != null){ //next position is not null, we found our next Node
					foundNewNode = true; //prepare to break loop
				}else{ //next position is empty, try one after that in next loop
					hash++; //try next position
				}
			}
			
			if(hash >= hashTable.length){ //we reached the end of the table return null
				return null;
			}else{ //we did not reach the end of the table set n to new Node
				n = hashTable[hash];
			}
		}
		lastNode = n; //set lastNode to n, the Node we found
		return n; //return the Node we found
	}
	
	/**
	 * Returns the first Node in the table, sets lastNode to first Node
	 * @return Returns the first Node in the table
	 */
	private Node getFirstNode(){
		int hash = 0; //start at beginning of table
		while(hashTable[hash] == null && hash < hashTable.length){ //loop while we haven't found a Node and we haven't reached the end of the table
			hash++;
		}
		
		if(hash >= hashTable.length){ //we reached the end of the table, there are no Nodes
			return null;
		}
		
		lastNode = hashTable[hash]; //set lastNode the the Node we found
		return hashTable[hash]; //return the Node we found
	}
	
	/**
	 * Adds the key to the hash table.
	 * If there is a collision, it should be dealt with by chaining the keys together.
	 * If the key is already in the hash table, it increments that key's counter.
	 * @param keyToAdd : the key which will be added to the hash table
	 */
	public void insert(String keyToAdd){
		//TODO Implement insert into the hash table.
		//If keyToAdd is already in the hash table, then increment its count.
		int hash = hash(keyToAdd);
		Node foundNode = findNode(keyToAdd);
		
		if(foundNode == null){ //Node was not found, it does not currently exist
			if(hashTable[hash] == null){ //root is null, start a list at root with new Node
				hashTable[hash] = new Node(keyToAdd);
			}else{ //root not null, make new Node as new root
				Node newNode = new Node(keyToAdd);
				newNode.next = hashTable[hash];
				hashTable[hash] = newNode;
			}
		}else{ //Node found, increment count
			foundNode.count++;
		}
	}
	
	/**
	 * Returns the number of times a key has been added to the hash table.
	 * @param keyToFind : The key being searched for
	 * @return returns the number of times that key has been added.
	 */
	public int findCount(String keyToFind){
		//TODO Implement findCount such that it returns the number of times keyToFind
		// has been added to the data structure.
		Node foundNode = findNode(keyToFind);
		if(foundNode == null){ //Node not found, word has appeared 0 times
			return 0;
		}
		return foundNode.count; //Node found, return count
	}
	
	/**
	 * Returns the Node of the keyToFind
	 * @param keyToFind : The key being searched for
	 * @return returns the Node. Returns null if a matching Node is not found
	 */
	private Node findNode(String keyToFind){
		int hash = hash(keyToFind);
		Node foundNode = hashTable[hash];
		
		if(foundNode == null){ //root is null, Node not found
			return null;
		}else if(foundNode.key.equals(keyToFind)){ //root is the Node we want
			return hashTable[hash];
		}
		else{ //root is not the Node we want
			//Go through list until we find a matching Node
			while(!foundNode.key.equals(keyToFind)){
				if(foundNode.next == null){ //no more Nodes in the list, Node not found
					return null;
				}
				foundNode = foundNode.next; //Try again with the next Node in the list
			}
			return foundNode; //Node found, return it
		}
	}

	/**
	 * Returns the hash of keyToHash
	 * @param keyToHash: String to hash
	 * @return Returns the hash of keyToHash
	 */
	private int hash(String keyToHash){
		//return Math.abs(keyToHash.hashCode() % hashTable.length);
		//EXTRA CREDIT: Implement your own String hash function here.
		
		//start at 0
		int hash = 0;
		
		//modify hash based on each character of the input string
		for(int i = 0; i < keyToHash.length(); i++){
			hash = 37 * hash + keyToHash.charAt(i);
		}
		
		//make sure hash fits in the table
		hash %= hashTable.length;
		
		//make sure hash is not negative
		if( hash < 0 ){
			hash += hashTable.length;
		}

		return hash;
	}
}
