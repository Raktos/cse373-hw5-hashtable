public class QPHash {
	Node[] hashTable;
	int lastHash;
	double tableSize;
	
	private class Node{
		String key;
		int count;
		
		private Node(String k){
			this.key = k;
			this.count = 1;
		}
	}
	
	public QPHash(){
		//TODO Implement a default constructor for QPHash
		this(1811);
	}
	
	public QPHash(int startSize){
		//TODO Implement a constructor that instantializes the hash array to startSize.
		hashTable = new Node[startSize];
		this.lastHash = -1;
		this.tableSize = 0;
	}

	/**
	 * This function allows rudimentary iteration through the QPHash.
	 * The ordering is not important so long as all added elements are returned only once.
	 * It should return null once it has gone through all elements
	 * WARNING inserting after this has been run will result in potentially missing nodes
	 * WARNING rehashing will reset this completly
	 * @return Returns the next element of the hash table. Returns null if it is at its end.
	 */
	public String getNextKey(){
		//TODO returns the next key in the hash table.
		//You will need external tracking variables to account for this.
		
		//start at minimum 1 greater than the last time
		int hash = lastHash + 1;
		
		while(hash < hashTable.length){ //loop until we're past the end of the table
			if(hashTable[hash] != null){ //we found the next Node
				lastHash = hash; //set lastHash to the one we just found
				return hashTable[hash].key;
			}else{ //couldn't find Node, try next position
				hash++;
			}
		}
		return null; //reached end of table
	}
	/**
	 * Adds the key to the hash table.
	 * If there is a collision, a new location should be found using quadratic probing.
	 * If the key is already in the hash table, it increments that key's counter.
	 * @param keyToAdd : the key which will be added to the hash table
	 */
	public void insert(String keyToAdd){
		//TODO Implement insert into the hash table.
		//If keyToAdd is already in the hash table, then increment its count.
		int hash = hash(keyToAdd);
		
		if(hashTable[hash] == null){ //position is null, put new Node here
			hashTable[hash] = new Node(keyToAdd);
			tableSize++;
		}else{ //position not null
			int probe = 1;
			boolean inserted = false;
			int nextHash = hash;
			
			//loop until we insert the key
			while(!inserted){
				if(hashTable[nextHash] == null){ //position empty, we can put it here
					hashTable[nextHash] = new Node(keyToAdd);
					tableSize++;
					inserted = true;
				}else if(hashTable[nextHash].key.equals(keyToAdd)){ //position matches attempted insert, increment count
					hashTable[nextHash].count++;
					inserted = true;
				}else{ //if we haven't inserted try again with new hash
					nextHash = (hash + (probe * probe)) % hashTable.length; //increase position by probe
					probe++; //increment probe base for next try
				}
			}
		}
		
		//table is too small, rehash
		if((tableSize / hashTable.length) >= 0.5){
			rehash();
		}
	}
	/**
	 * Returns the number of times a key has been added to the hash table.
	 * @param keyToFind : The key being searched for
	 * @return returns the number of times that key has been added.
	 */
	public int findCount(String keyToFind){
		//TODO Implement findCount such that it returns the number of times keyToFind
		// has been added to the data structure.
		int hash = findKey(keyToFind);
		
		if(hash < 0){ //key not found, count is 0
			return 0;
		}else{ //key found return count
			return hashTable[hash].count;
		}
	}
	
	/**
	 * Returns the hash (position) of a key
	 * @param keyToFind : The key being searched for
	 * @return returns the hash of that key, returns -1 if the key cannot be found
	 */
	private int findKey(String keyToFind){
		int hash = hash(keyToFind);
		int probe = 1;
		int nextHash = hash;
		
		//loop until we confirm the key is there or not
		while(true){
			if(hashTable[nextHash] == null){ //found an empty slot, key must not be in table
				return -1;
			}else if(hashTable[nextHash].key.equals(keyToFind)){ //found key, return current position
				return nextHash;
			}else{ //wrong key, try next probe position
				nextHash = (hash + (probe * probe)) % hashTable.length;
				probe++;
			}
		}
	}

	/**
	 * Returns the hash of keyToHash
	 * @param keyToHash: String to hash
	 * @return Returns the hash of keyToHash
	 */
	private int hash(String keyToHash){
		//return Math.abs(keyToHash.hashCode() % hashTable.length);
		//EXTRA CREDIT: Implement your own String hash function here.
		
		//start at 0
		int hash = 0;
		
		//modify hash based on each character of the input string
		for(int i = 0; i < keyToHash.length(); i++){
			hash = 37 * hash + keyToHash.charAt(i);
		}
		
		//make sure hash fits in the table
		hash %= hashTable.length;
		
		//make sure hash is not negative
		if( hash < 0 ){
			hash += hashTable.length;
		}

		return hash;
	}
	
	/**
	 * creates a new table of ~double the size
	 * WARNING resets lastHash thus resetting findNextKey to the start
	 */
	private void rehash(){
		//store old table and create a new one
		//the new table length is the closest prime number to 2x the old tables length
		Node[] oldHashTable = hashTable;
		hashTable = new Node[getNextPrime(oldHashTable.length * 2)];
		tableSize = 0;
		
		//populate the new table with the old table's contents
		for(int i = 0; i < oldHashTable.length; i++){
			if(oldHashTable[i] != null){
				insert(oldHashTable[i].key);
				hashTable[findKey(oldHashTable[i].key)].count = oldHashTable[i].count; //set the count in the new table to the one that was in the old table
			}
		}
		//reset lastHash, it's useless with a new table
		lastHash = -1;
	}
	
	/**
	 * Returns the next prime number from the input
	 * @param n: starting integer
	 * @return returns the closest prime number larger than n
	 */
	private int getNextPrime(int n)
	{
	    n++; //start at a value bigger than input
	    
	    //make sure n is not even
	    if(n % 2 == 0){
	    	n++;
	    }
	    
	    //check each subsequent odd number to see if it is prime, mathematically it is guaranteed to be less than 2n
	    for (int i = n; i < 2 * n; i += 2)
	    {
	    	if(isPrime(i)){ //found a prime, return it
	    		return i;
	    	}
	    }
	    return -1; //mathematics broke, return -1
	}
	
	/**
	 * Returns if a number is prime or not
	 * @param n: number to check
	 * @return true/false if a number is prime
	 */
	private boolean isPrime(int n){
		if(n == 1 || n == 2){ //clear special cases
			return true;
		}
		
		if (n % 2 == 0){ //n is even, cannot be prime
        	return false;
        }  
		
        for (int i = 3; i * i <= n; i += 2){ //check general case
        	if (n % i == 0){
        		return false;
        	}
        }
        return true;
	}
}
